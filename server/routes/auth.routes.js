import express from "express";
import {
  registerPost,
  registerGet,
  login,
  add_dishes,
} from "../controllers/auth.controllers.js";
const router = express.Router();

router.post("/register", registerPost);
router.get("/register", registerGet);

router.post("/login", login);
router.post("/add-dishes", add_dishes);

export default router;
