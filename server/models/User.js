import mongoose from "mongoose";

const userSchema = mongoose.Schema({
  email: String,
  password: String,
});

const UserSchema = mongoose.model("UserSchema", userSchema);
export default UserSchema;
