import mongoose from "mongoose";

const dishesSchema = mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  kcal: { type: String, required: true },
});

const DishesSchema = mongoose.model("DishesSchema", dishesSchema);
export default DishesSchema;
