import UserModel from "../models/User.js";
import DishModel from "../models/Dishes.js";
import bcrypt from "bcrypt";
import nodemailer from "nodemailer";

const htmlMessage = (url = "") => `<div>Вы успешно зарегестрированы в ketodiet 🐣!</div><a href=${url}><button>Перейти</button></a>`;

export const registerPost = async (req, res) => {
  try {
    const { email, password } = req.body;
    const hashedPassword = bcrypt.hashSync(password, 12);
    const Candidate = await UserModel.findOne({ email });
    if (Candidate) {
      res.status(400).json({
        wiadomosc: "taki email juz zarejstrowany",
      });
      return;
    }

    // @ts-ignore
    const user = UserModel({
      email: email,
      password: hashedPassword,
    });
    // @ts-ignore

    await sendEmail(email, password);

    user.save().then((result) => {
      res.status(200).json({ wiadomosc: "uzytkownil zostal utwotzony" });
    });
  } catch (e) {
    res.status(404).json({
      wiadomosc: `${e}`,
    });
  }
};

// @ts-ignore
export const registerGet = async (req, res) => {
  UserModel.find().then((result) => {
    res.status(200).json({
      info: result,
    });
  });
};

export const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await UserModel.findOne({ email });
    if (!user) {
      res.status(400).json({ wiadomosc: "taki uzytkownik nie istnieje" });
    }
    // @ts-ignore
    const validatePassword = bcrypt.compare(password, user.password);
    if (!validatePassword) {
      res.status(400).json({ wiadomosc: "nieprawidlowe haslo" });
    }

    res.json({ userId: user.id });
  } catch {
    res.status(404).json({
      // @ts-ignore
      wiadomosc: `${e}`,
    });
  }
};

export const add_dishes = async (req, res) => {
  try {
    const { name, description, kcal } = req.body;
    const CandidateDish = await DishModel.findOne({ name });
    if (CandidateDish) {
      res.status(400).json({
        wiadomosc: "takie jedzenie juz jest",
      });
    }
    // @ts-ignore
    const dish = DishModel({
      name: name,
      description: description,
      kcal: kcal,
    });
    // @ts-ignore
    dish.save().then((result) => {
      res.status(200).json({ wiadomosc: "jedzenie zostalo utworzone" });
    });
  } catch (e) {
    res.status(404).json({
      wiadomosc: `${e}`,
    });
  }
};

export const sendEmail = async (email, pass) => {
  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "ketodietV.1.0@gmail.com",
      pass: "ketodietVABA",
    },
  });

  await transporter.sendMail({
    from: '"Fred Foo 👻" <kirillll13562@gmail.com>',
    to: email,
    subject: "Дратути 👻",
    text: "Вы успешно зарегестрированы в ketodiet",
    html: htmlMessage()
  });
};
