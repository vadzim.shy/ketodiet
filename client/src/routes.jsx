import React from "react";
import { Switch, Route } from "react-router-dom";
import StartPage from "./pages/InputInfo";
import Complete from "./pages/Complete";
import AuthPage from './pages/AuthPage'

export const useRoutes = (isAuthenticated) => {
  if (isAuthenticated) {
    return (
      <Switch>
        <Route path="/auth">
        <AuthPage />
      </Route>
      </Switch>
    );
  }
  return (
    <Switch>
      <Route path="/complete">
        <Complete />
      </Route>
      <Route path="/">
        <StartPage />
      </Route>
    </Switch>
  );
};
