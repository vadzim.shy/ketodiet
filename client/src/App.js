import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { useRoutes } from "./routes.jsx";

function App() {
  const routes = useRoutes(true);
  return (
    <Router>
      <div className="container">{routes}</div>
    </Router>
  );
}

export default App;
