import React, { useState } from "react";
import "./InputInfo.css";
import { Link } from "react-router-dom";
import {changeHandler} from '../../hooks/FormHandlerMethod'


const DietForm = () => {
  const {foo} = changeHandler();
  const [values, setValues] = useState({
    age: 0,
    weight: 0,
    height: 0,
    activity: 1.2,
    gender: "",
  });

  const Submit = () => {
      localStorage.setItem("dietOptions",JSON.stringify({
      age:  values.age,
      weight: values.weight,
      height:  values.height,
      activity:  values.activity,
      gender: values.gender
    }));
  }
  const handleChange = (event) => {
    event.persist();
    foo(values,setValues,event.target.name,event.target.value)
  }
  return (
    <form>
      <div className="container">
        <div>
          <label htmlFor="age">
            Age:
            <input
              id="age"
              name="age"
              type="number"
              value={values.age}
              onChange={handleChange}
            ></input>
          </label>
        </div>
        <div>
          <label htmlFor="weight">
            weight:
            <input
              id="weight"
              name="weight"
              type="number"
              value={values.weight}
              onChange={handleChange}
            ></input>
          </label>
        </div>
        <div>
          <label htmlFor="height">
            height:
            <input
              id="height"
              name="height"
              type="number"
              value={values.height}
              onChange={handleChange}
            ></input>
          </label>
        </div>
        <div>
          <select name="activity" onChange={handleChange}>
            <option value={1.2}>минимум/отсутствие физической нагрузки</option>
            <option value={1.375}>3 раза в неделю</option>
            <option value={1.4625}>5 раз в неделю</option>
            <option value={1.550}>5 раз в неделю (интенсивно)</option>
            <option value={1.6375}>Каждый день</option>
          </select>
        </div>
        <div onChange={handleChange}>
          <input type="radio" value="Male" name="gender" /> Male
          <input type="radio" value="Female" name="gender" /> Female
          <input type="radio" value="Other" name="gender" /> Other
        </div>
        <Link to="/complete" onClick={Submit} className="btn btn-primary">
          hello
        </Link>
      </div>
    </form>
  );
};

export default DietForm;
