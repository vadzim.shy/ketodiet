import React from "react";
import { count } from "../../utils/calculateCount";

const CompleteComponent = () => {
  const result = count();

  return <div>{result}</div>;
};

export default CompleteComponent;
