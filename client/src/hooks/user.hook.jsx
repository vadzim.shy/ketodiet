export const RegisterHandler = () => {
  const request = async (url, method, body = null, headers = {}) => {
    try {
      if (body) {
        body = JSON.stringify(body);
        headers["Content-type"] = "application/json";
      }
      const response = await fetch(url, {
        method,
        body,
        headers,
      });
      const data = await response.json();

      if (!response.ok) {
        throw new Error(data.message || "Error");
      }

      return data;
    } catch {}
  };
  return { request };
};
