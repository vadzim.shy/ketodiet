import React, { useState } from "react";
import { RegisterHandler } from "../hooks/user.hook";
import {changeHandler} from '../hooks/FormHandlerMethod'
const {foo} = changeHandler();
const AuthPage = () => {
  const { request } = RegisterHandler();
  const [form, setForm] = useState({
    email: "",
    password: "",
  });
 
  const Input = (event) =>{
    foo(form,setForm,event.target.name, event.target.value)
  }
   const registerHandler = async () => {
    request("/auth/register", "POST", { ...form });
  };

  return (
    <div className="row">
      <div className="col s6 offset-s3">
        <h1>Сократи ссылку</h1>
        <div className="card blue darken-1">
          <div className="card-content white-text">
            <span className="card-title">Autorization</span>
            <div>
              <div className="input-field ">
                <input
                  placeholder="Enter email"
                  id="email"
                  type="text"
                  name="email"
                  className="yellow-input"
                  onChange={Input}
                />
                <label htmlFor="email">email</label>
              </div>
              <div className="input-field ">
                <input
                  placeholder="Enter password"
                  id="password"
                  type="password"
                  name="password"
                  className="yellow-input"
                  onChange={Input}
                />
                <label htmlFor="password">password</label>
              </div>
            </div>
          </div>
          <div className="card-action">
            <button className="btn yellow darken-4">Войти</button>
            <button className="btn yellow darken-4" onClick={registerHandler}>
              Регистрация
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
export default AuthPage;
