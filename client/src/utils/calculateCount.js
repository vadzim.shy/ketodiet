export const count = () => {
  const { weight, height, age, activity, gender } = JSON.parse(
    localStorage.getItem("dietOptions")
  );

  if (gender === "Male") {
    return (10 * weight + (6.25 * height - 5 * age) + 5) * activity;
  }

  return (10 * weight + (6.25 * height - 5 * age) - 161) * activity;
};
